﻿using InventoryMgmt.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using InventoryMgmt.Repository;
using InventoryMgmt.Models;
using System.IO;
using InventoryModel.Model;

namespace InventoryMgmt.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetAllProducts()
        {
            try
            {

                List<VMProduct> objProduct = new List<VMProduct>();
                using (ProductRepository repo = new ProductRepository())
                {
                    objProduct = repo.GetAllProducts("productapi/getallproducts", Session["Token"].ToString());

                }
                ViewBag.lstProduct = objProduct;
                return View();
            }

            catch (Exception ex)
            {
                Logger.LogError("ProductController:GetAllProducts", ex);
                return View();
            }
        }
        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                int ProductId = 0;
                VMProduct obj = new VMProduct();
                using (ProductRepository repo = new ProductRepository())
                {

                    if (Request.QueryString != null)
                    {
                        if (Request.QueryString["PId"] != null && Request.QueryString["PId"] != "")
                        {
                            ProductId = Convert.ToInt32(Request.QueryString["PId"]);

                            obj = repo.GetProductById("productapi/getproductbyid?id=" + ProductId, Session["Token"].ToString());
                        }
                    }

                }

                return View(obj);
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductController:Create", ex);
                return View();
            }
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(VMProduct model, string ImageDetail, string Desc)
        {
            try
            {
                if (ImageDetail != "[]")
                {
                    var Image = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VMProduct>>(ImageDetail);
                    model.Image = Image[0].Attachment.Replace(' ', '+');
                }


                if (!string.IsNullOrEmpty(Desc))
                    model.Descrption = Desc;

                if (model.ProductId > 0)
                {
                    model.ModifiedBy = Convert.ToInt32(Session["UserId"].ToString());
                }
                else
                    model.CreatedBy = Convert.ToInt32(Session["UserId"].ToString());
                if (ModelState.IsValid)
                {
                    using (ProductRepository repo = new ProductRepository())
                    {
                        HttpResponseMessage response = repo.InsertUpdateProduct("productapi/insertupdateproduct?UserId=" + Convert.ToInt32(Session["UserId"].ToString()), model, Session["Token"].ToString());
                        if (response != null && response.IsSuccessStatusCode == true)
                        {
                            return Json(new { success = "Product detail save successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { error = response.RequestMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }


                return Json(new { error = "Error! Please try again!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductController:Create", ex);
                return Json(new
                {
                    error = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ProductDetail()
        {
            VMProduct obj = new VMProduct();
            try
            {
                int ProductId = 0;
                using (ProductRepository repo = new ProductRepository())
                {
                    if (Request.QueryString != null)
                    {
                        if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
                        {
                            ProductId = Convert.ToInt32(Request.QueryString["id"]);
                            obj = repo.GetProductById("productapi/getproductbyid?id=" + ProductId, Session["Token"].ToString());
                        }
                    }
                }

                return View(obj);
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductController:DeleteProduct", ex);
                return View(obj);
            }
        }
        [HttpPost]
        public ActionResult DeleteProduct(int ProductId)
        {
            try
            {
                using (ProductRepository repo = new ProductRepository())
                {
                    HttpResponseMessage response = repo.DeleteProduct("productapi/deleteproduct?id=" + ProductId + "&UserId=" + Convert.ToInt32(Session["UserId"].ToString()), Session["Token"].ToString());
                    if (response != null && response.IsSuccessStatusCode == true)
                    {
                        return Json(new { success = "Product deleted successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = response.RequestMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductController:DeleteProduct", ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}