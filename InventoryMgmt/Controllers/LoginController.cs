﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InventoryMgmt.Common;
using InventoryMgmt.Models;
using InventoryMgmt.Repository;

namespace InventoryMgmt.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
      
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(VMLogin objUser)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    TempData["Inactive"] = "Inactive";
                    TempData["Login_Status"] = "Invalid User.";
                    return View();
                }
                Token objToken = new Token();
                using (LoginRepository repo = new LoginRepository())
                {
                    var form = new Dictionary<string, string>
                       {
                           {"grant_type", "password"},
                           {"username", objUser.UserName},
                           {"password", objUser.UserPassword},
                       };
                    objToken = repo.ValidateUserLogin("token", form);
                    if (objToken != null)
                    {
                       
                        Session["Token"] = objToken.AccessToken;
                        VMLogin user = new VMLogin();

                        user = repo.GetLoginUserDetail("loginapi/GetLoginUserDetail?username=" + objUser.UserName + "&userpassowrd=" + objUser.UserPassword, Session["Token"].ToString());

                        Session["UserId"] = user.Id;
                        return RedirectToAction("GetAllProducts", "Product");
                    }
                    else
                    {

                        TempData["Inactive"] = "Inactive";
                        TempData["Login_Status"] = "Invalid UserName Or Password.";
                        return View();
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError("LoginController:Login", ex);
                throw;
            }
        }

        public ActionResult LogOff()
        {
            Session["Token"] = string.Empty;
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Login", "Login");
        }
    }
}