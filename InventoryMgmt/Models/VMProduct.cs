﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventoryMgmt.Models
{
    public class VMProduct
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Descrption { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string Attachment { get; set; }
        public string Image { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
    }
}