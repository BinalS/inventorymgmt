﻿using InventoryMgmt.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using InventoryMgmt.Models;

namespace InventoryMgmt.Repository
{
    public class ProductRepository : IDisposable
    {
        public HttpClient Client { get; set; }
        public ProductRepository()
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ServiceUrl"].ToString());
            
        }
        public List<VMProduct> GetAllProducts(string url,string token)
        {
            try
            {
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token);
                System.Net.Http.HttpResponseMessage response = Client.GetAsync(url).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<List<VMProduct>>().Result;
                return null;
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductRepository:GetAllProducts", ex);
                return null;
            }
        }

        
        public HttpResponseMessage InsertUpdateProduct(string url, object model, string token)
        {
            try
            {
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token);
                System.Net.Http.HttpResponseMessage response = Client.PutAsJsonAsync(url, model).Result;

                if (response.IsSuccessStatusCode)
                    return response;
                return null;
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductRepository:InsertUpdateProduct", ex);
                return null;
            }
        }
        public VMProduct GetProductById(string url, string token)
        {
            try
            {
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token);
                System.Net.Http.HttpResponseMessage response = Client.GetAsync(url).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<VMProduct>().Result;
                return null;
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductRepository:GetProductById", ex);
                return null;
            }
        }
        public HttpResponseMessage DeleteProduct(string url,string token)
        {
            try
            {
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token);
                System.Net.Http.HttpResponseMessage response = Client.DeleteAsync(url).Result;

                if (response.IsSuccessStatusCode)
                    return response;
                return null;
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductRepository:DeleteProduct", ex);
                return null;
            }
        }
        public void Dispose()
        {

        }
    }
}