﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Net.Http.Formatting;
using InventoryMgmt.Common;
using InventoryMgmt.Models;

namespace InventoryMgmt.Repository
{
    public class LoginRepository :IDisposable
    {
        public void Dispose()
        {

        }
        public HttpClient Client { get; set; }
        public LoginRepository()
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ServiceUrl"].ToString());
        }
        public Models.Token ValidateUserLogin(string url,Dictionary<string,string>form )
        {
            try
            {
                var tokenResponse = Client.PostAsync(url, new FormUrlEncodedContent(form)).Result;

                Token tokenobj = new Token();
                tokenobj = tokenResponse.Content.ReadAsAsync<Token>(new[] { new JsonMediaTypeFormatter() }).Result;

                if (tokenResponse.IsSuccessStatusCode)
                {
                    return tokenobj;
                }
                return null;
               
            }
            catch (Exception ex)
            {
                Logger.LogError("LoginRepository:Validate", ex);
                return null;
            }
        }

        public VMLogin GetLoginUserDetail(string url, string token)
        {
            try
            {
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token);
                System.Net.Http.HttpResponseMessage response = Client.GetAsync(url).Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<VMLogin>().Result;
                return null;
            }
            catch (Exception ex)
            {
                Logger.LogError("LoginRepository:GetLoginUserDetail", ex);
                return null;
            }
        }
    }
}