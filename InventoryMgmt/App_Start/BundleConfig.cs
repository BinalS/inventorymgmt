﻿using System.Web;
using System.Web.Optimization;

namespace InventoryMgmt
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/datatables").Include(
                      "~/Scripts/dataTables/jquery.dataTables.min.js",
                      "~/Scripts/dataTables/extensions/dataTables.bootstrap.min.js",
                      "~/Scripts/dataTables/extensions/dataTables.responsive.min.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/bundles/fontawesome")
             .Include("~/Content/fontawesome/css/font-awesome.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/alertify").Include(
                   "~/Content/alertifyjs/css/alertify.min.css",
                   "~/Content/alertifyjs/css/themes/default.min.css"));
            bundles.Add(new ScriptBundle("~/js/alertify").Include(
                    "~/Content/alertifyjs/alertify*"));

            bundles.Add(new StyleBundle("~/bundles/datatables/css").Include(
               "~/Content/dataTables/dataTables.bootstrap.min.css",
               "~/Content/dataTables/responsive.dataTables.min.css"));
        }
    }
}
