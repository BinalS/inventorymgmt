﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace InventoryMgmt.Common
{
    public static class Logger
    {
        private static readonly Page Pge = new Page();
        private static readonly string Path =System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "ErrorLog\\Log.txt";// Pge.Server.MapPath("~/ErrorLog/Log.txt");
        private const string LineBreaker = "\r\n\r======================================================================================= \r\n\r";

        public static void LogError(string myMessage, Exception e)
        {
            //Create a writer and open the file:
            System.IO.StreamWriter log;

            //const LogSeverity severity = LogSeverity.Error;
            string messageToWrite = string.Format("{0} {1}: {2} \r\n\r {3}\r\n\r {4}{5}", DateTime.Now, "Blank", myMessage, e.Message, e.StackTrace, LineBreaker);
            if (!System.IO.File.Exists(Path))
            {
                log = new System.IO.StreamWriter(Path);
            }
            else
            {
                log = System.IO.File.AppendText(Path);
            }
            //System.IO.File.AppendAllText(Path, messageToWrite);
            // Write to the file:
            log.WriteLine("-------------------------------------------------------------");
            log.WriteLine(DateTime.Now);
            log.WriteLine("Exception :" + (e.Message != null ? e.Message : "NULL"));
            log.WriteLine("Inner Exception :" + (e.InnerException != null ? e.InnerException.Message : "NULL"));
            log.WriteLine("StackTrace" + messageToWrite);
            log.WriteLine("-------------------------------------------------------------");
            log.WriteLine();

            // Close the stream:
            log.Close();

        }
    }
}
