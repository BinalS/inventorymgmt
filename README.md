# Inventory Management

In order to prepare the InventoryManagement from the scratch, the first step was to go through the tasks and the requirements. After carefully understanding the system requirement, the next step was to prepare rough flow diagrams. This helped me to prepare the database and understand the flow of the system. The project called Inventory Management is prepared to fulfill the given tasks.

**Database Design**:

To prepare the database, I have choose SQL Server.

Now, to start preparing the database, first I prepare the schema and required tables. File called DbInventory.sql contains statements for creating the database.


**Models**:

Once the database has been created, the next step is to prepare the models corresponding to the tables using Database First Approach of EntityFramework.

**Database Connection**:

Database connection has been established using  EntityFramework

**Database Operations**:

The code to perform required database operations such as insert, update, delete and select is written using Entity Framework.

**Methods**:

The required methods for the given tasks are prepared in InventoryMgmtServices using Web APIs. 

**FrontEnd**:

The Frontend of Project is developed using ASP.NET MVC C#. 

**TestCases**:

In order to prepare the required test cases for methods, unit test project is used. 


**Steps To compile the project**:

1. In order to compile and run the project,first clone the project.
2. Open project in Visual Studio.
3. Set InventoryMgmt Project as Startup Project.
4. Run Project by clicking on IIS Express.