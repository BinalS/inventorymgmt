﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryModel.Services.IServices;
using InventoryModel.Repository;
using InventoryModel.Model;

namespace InventoryModel.Services
{
    public class ProductService : IProductService
    {
        public List<Product> GetAllProducts()
        {
            return ProductRepo.GetAllProducts();
        }
        public Product GetProductById(int id)
        {
            return ProductRepo.GetProductById(id);
        }
        public bool InsertUpdateProduct(int UserId, Product productItem)
        {
            return ProductRepo.InsertUpdateProduct(UserId, productItem);
        }
        public bool DeleteProduct(int id, int UserId)
        {
            return ProductRepo.DeleteProduct(id, UserId);
        }
        public UserLogin GetLoginUserDetail(string UserName, string Password)
        {
            return LoginRepo.GetLoginUserDetail(UserName, Password);
        }
    }
}
