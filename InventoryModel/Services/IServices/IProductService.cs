﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryModel.Model;

namespace InventoryModel.Services.IServices
{
    public interface IProductService
    {
        List<Product> GetAllProducts();
        Product GetProductById(int id);
        bool InsertUpdateProduct(int UserId, Product productItem);
        bool DeleteProduct(int id, int UserId);
        UserLogin GetLoginUserDetail(string Username, string Password);
    }
}
