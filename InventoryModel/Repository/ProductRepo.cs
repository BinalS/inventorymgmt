﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using InventoryModel.Model;

namespace InventoryModel.Repository
{
    public class ProductRepo : IDisposable
    {
        public static List<Product> GetAllProducts()
        {
            List<Product> Products = new List<Product>();
            try
            {
                using (dbInventoryEntities ctx = new dbInventoryEntities())
                {
                    List<Product> ProductList = ctx.Products.Where(x => x.IsActive == true).OrderByDescending(x => x.ProductId).ToList();
                    foreach (var item in ProductList)
                    {
                        Product objP = new Product();
                        objP.ProductId = item.ProductId;
                        objP.Name = item.Name;
                        objP.Descrption = item.Descrption;
                        objP.Price = item.Price;
                        objP.Image = item.Image;

                        Products.Add(objP);
                    }
                     return Products.ToList();
                }
            }
            catch (Exception)
            {
                return Products.ToList();
            }
        }

        public static Product GetProductById(int id)
        {
            try
            {
                using (dbInventoryEntities ctx = new dbInventoryEntities())
                {
                    bool IsProductExist = ctx.Products.Any(x => x.ProductId == id);
                    if (IsProductExist)
                    {
                        var Product = ctx.Products.First(x => x.ProductId == id);
                        Product objP = new Product();
                        objP.ProductId = Product.ProductId;
                        objP.Name = Product.Name;
                        objP.Descrption = Product.Descrption;
                        objP.Price = Product.Price;
                        objP.Image = Product.Image;

                        return objP;
                    }
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool InsertUpdateProduct(int UserId, Product productItem)
        {
            try
            {
                using (dbInventoryEntities ctx = new dbInventoryEntities())
                {
                    bool IsProductExist = ctx.Products.Any(x => x.ProductId == productItem.ProductId);

                    if (IsProductExist)
                    {
                        var Pobj = ctx.Products.Where(x => x.ProductId == productItem.ProductId).FirstOrDefault();

                        Pobj.Name = productItem.Name;
                        Pobj.Descrption = productItem.Descrption;
                        Pobj.Price = productItem.Price;
                        Pobj.Image = productItem.Image;
                        Pobj.IsActive = true;
                        Pobj.ModifiedDate = DateTime.Now;
                        Pobj.ModifiedBy = UserId;
                        ctx.Entry(Pobj).State = EntityState.Modified;
                        ctx.SaveChanges();

                        return  true;
                    }
                    else
                    {
                        Product Pobj = new Product();
                        Pobj.Name = productItem.Name;
                        Pobj.Descrption = productItem.Descrption;
                        Pobj.Price = productItem.Price;
                        Pobj.Image = productItem.Image;
                        Pobj.IsActive = true;
                        Pobj.CreatedDate = DateTime.Now;
                        Pobj.CreatedBy = UserId;
                        ctx.Entry(Pobj).State = EntityState.Added;
                        ctx.SaveChanges();

                        return true;
                    } 
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        //Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }

                throw;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool DeleteProduct(int id, int UserId)
        {
            bool status = false;
            try
            {
                using (dbInventoryEntities ctx = new dbInventoryEntities())
                {
                    Product Pobj = ctx.Products.Where(p => p.ProductId == id).FirstOrDefault();
                    if (Pobj != null)
                    {
                        Pobj.IsActive = false;
                        Pobj.ModifiedDate = DateTime.Now;
                        Pobj.ModifiedBy = UserId;
                        ctx.Entry(Pobj).State = EntityState.Modified;
                        ctx.SaveChanges();
                    }
                    status = true;
                }
            }
            catch (Exception)
            {
                status = false;
            }
            return status;
        }

        public void Dispose()
        {

        }
    }
}
