﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryModel.Model;

namespace InventoryModel.Repository
{
    public class LoginRepo :IDisposable
    {
        static dbInventoryEntities DbContext;
        static LoginRepo()
        {
            DbContext = new dbInventoryEntities();
        }
        public void Dispose()
        {

        }
        public UserLogin ValidateUserLogin(string username, string password)
        {
            var login = DbContext.UserLogins.Where(user =>
            user.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
            && user.UserPassword == password).FirstOrDefault();
            return login;
        }
        public static UserLogin GetLoginUserDetail(string UserName, string Password)
        {
            try
            {
                bool IsUserExists = DbContext.UserLogins.Any(x => x.UserName == UserName && x.UserPassword == Password);
                if (IsUserExists)
                {
                    UserLogin objU = DbContext.UserLogins.Where(x => x.UserName == UserName && x.UserPassword == Password).FirstOrDefault();
                    return objU;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
