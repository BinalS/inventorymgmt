﻿using InventoryModel.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using InventoryModel.Model;
using InventoryMgmtServices.Common;
using InventoryModel.Repository;

namespace InventoryMgmtServices.Controllers
{
    [Authorize]
    public class LoginAPIController : ApiController
    {
        private readonly IProductService _productServices;

        public LoginAPIController(IProductService productService)
        {
            this._productServices = productService;
        }

        [HttpGet]
        public IHttpActionResult GetLoginUserDetail(string username, string userpassowrd)
        {
            try
            {
                UserLogin objUser = _productServices.GetLoginUserDetail(username, userpassowrd);
                if (objUser == null)
                {
                    return NotFound();
                }

                return Ok(objUser);
            }
            catch (Exception ex)
            {
                Logger.LogError("LoginAPI:GetLoginUserDetail", ex);
                return InternalServerError();
            }
        }
    }
}
