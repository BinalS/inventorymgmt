﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using InventoryMgmtServices.Common;
using InventoryModel.Services.IServices;
using InventoryModel.Model;
using Newtonsoft.Json;
using InventoryModel.Repository;
using System.Threading.Tasks;
using Microsoft.Owin.Security.Provider;

namespace InventoryMgmtServices.Controllers
{
    [Authorize]
    public class ProductAPIController : ApiController
    {

        private readonly IProductService _productServices;

        public ProductAPIController(IProductService productService)
        {
            this._productServices = productService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetAllProducts()
        {
            try
            {
                System.Diagnostics.Debugger.Break();
                List<Product> lstProduct = await Task.FromResult(_productServices.GetAllProducts());
                if (lstProduct?.Count <= 0)
                {
                    return  NotFound();
                }

                  return Ok(lstProduct);
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductApi:GetAllProducts", ex);
                return InternalServerError();
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> GetProductById(int id)
        {
            try
            {
                Product objProduct = new Product();
                objProduct = await Task.FromResult(_productServices.GetProductById(id));
                if (objProduct == null)
                {
                    return NotFound();
                }

                return Ok(objProduct);
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductApi:GetProductById", ex);
                return InternalServerError();
            }
        }
        [HttpPut]
        public async Task<bool> InsertUpdateProduct(int UserId, Product product)
        {
            bool status = false;
            try
            {
                status = await Task.FromResult(_productServices.InsertUpdateProduct(UserId, product));
                return status;
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductApi:InsertUpdateProduct", ex);
                return status;
            }
        }
        [HttpDelete]
        public async Task<bool> DeleteProduct(int id, int UserId)
        {
            bool status = false;
            try
            {
                status = await Task.FromResult(_productServices.DeleteProduct(id, UserId));
                return status;
            }
            catch (Exception ex)
            {
                Logger.LogError("ProductApi:DeleteProduct", ex);
                return status;
            }
        }
    }
}
