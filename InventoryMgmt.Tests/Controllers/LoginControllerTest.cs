﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InventoryMgmtServices.Controllers;
using InventoryModel.Model;
using InventoryModel.Services.IServices;
using Moq;
using System.Web.Http;
using System.Web.Http.Results;
using InventoryModel.Repository;
using InventoryMgmtServices.Athentication;

namespace InventoryMgmt.Tests.Controllers
{
    [TestClass]
    public class LoginControllerTest
    {
        [TestMethod]
        public void GetUserDetail()
        {
            var mockRepository = new Mock<IProductService>();
            mockRepository.Setup(x => x.GetLoginUserDetail("admin", "123456"))
                .Returns(new UserLogin { Id = 1 });

            var controller = new LoginAPIController(mockRepository.Object);

            IHttpActionResult actionResult = controller.GetLoginUserDetail("admin", "123456");
            var contentResult = actionResult as OkNegotiatedContentResult<UserLogin>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(1, contentResult.Content.Id);
        }
        [TestMethod]
        public void GetUserDetailNotFound()
        {
            var mockRepository = new Mock<IProductService>();
            var controller = new LoginAPIController(mockRepository.Object);

            IHttpActionResult actionResult = controller.GetLoginUserDetail("admin1", "123456");

            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

    }
}
