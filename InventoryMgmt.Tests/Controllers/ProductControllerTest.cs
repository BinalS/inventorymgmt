﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InventoryMgmtServices.Controllers;
using InventoryModel.Model;
using InventoryModel.Services.IServices;
using System.Web.Mvc;
using InventoryModel.Services;
using Moq;
using System.Web.Http;
using System.Net;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InventoryMgmt.Tests.Controllers
{
    [TestClass]
    public class ProductControllerTest
    {
        [TestMethod]
        public async Task GetPrductById()
        {
            var mockRepository = new Mock<IProductService>();
            mockRepository.Setup(x => x.GetProductById(1))
                .Returns(new Product { ProductId = 1 });

            var controller = new ProductAPIController(mockRepository.Object);

            var actionResult = await controller.GetProductById(1);
            var contentResult = actionResult as OkNegotiatedContentResult<Product>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(1, contentResult.Content.ProductId);
        }
        [TestMethod]
        public async Task GetProductNotFound()
        {
            // Set up Prerequisites 
            var mockRepository = new Mock<IProductService>();
            var controller = new ProductAPIController(mockRepository.Object);

            // Act
            var actionResult = await controller.GetProductById(99);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public async Task GetAllProductNoFound()
        {
            // Set up Prerequisites 
            var mockRepository = new Mock<IProductService>();
            mockRepository.Setup(x => x.GetAllProducts())
               .Returns(new List<Product>());
            var controller = new ProductAPIController(mockRepository.Object);

            // Act
            var actionResult = await controller.GetAllProducts();

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public async Task GetAllProduct()
        {
            // Set up Prerequisites 
            var mockRepository = new Mock<IProductService>();
     
            var controller = new ProductAPIController(mockRepository.Object);

            // Act
            var actionResult = await controller.GetAllProducts();

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Product>>));
        }
        [TestMethod]
        public async Task AddProductTest()
        {
            // Arrange
            Product ObjPrd = new Product
            {
                Name = "Skirt",
                Price = 1000,
                Descrption = "Skrirt is best",
                Image = "",
                IsActive = true,
                CreatedBy = 1,
                CreatedDate = DateTime.Now
            };

            var mockRepository = new Mock<IProductService>();
            mockRepository.Setup(x => x.InsertUpdateProduct(1,ObjPrd)).Returns(true);
            var controller = new ProductAPIController(mockRepository.Object);

            // Act
            bool actionResult = await controller.InsertUpdateProduct(1,ObjPrd);

            // Assert
            Assert.IsNotNull(actionResult);
            Assert.AreEqual(true, actionResult);
        }
        [TestMethod]
        public async Task DeleteProductTest()
        {
            // Arrange
            var mockRepository = new Mock<IProductService>();
            mockRepository.Setup(x => x.DeleteProduct(1, 1)).Returns(true);
            var controller = new ProductAPIController(mockRepository.Object);

            // Act
            bool actionResult = await controller.DeleteProduct(1, 1);

            // Assert
            Assert.IsNotNull(actionResult);
            Assert.AreEqual(true, actionResult);
        }
    }
}
